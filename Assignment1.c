/*
 ============================================================================
 Name        : PreAssign_Sep20_Q_1.c
 Author      : Shubham awasarkar
 Version     :
 Copyright   : Open source
 Description : Input a string from user on command line. String may have multiple commas 
                e.g. "Welcome,to,Sunbeam,CDAC,Diploma,Course". Print each word individually. Hint: use strtok() function
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>

int main(int argcnt, char *string[]) {
	if(argcnt!=2)
		return 0;
	char *point = strtok(string[1],",");

	while(point !=NULL){
		printf("\n%s",point);
		point = strtok(NULL,",");
	}
	//NOTE:Original string gets manipulated after strtok() function
	return 0;
}
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define ITEM_DB "item.db"

typedef struct item{
    int id;
    char name[80];
    int price;
    double quantity;
}item_t;

int get_next_id(){
    item_t t;
    int max=0;
    long size = sizeof(item_t);
    FILE *fp;
    fp = fopen(ITEM_DB,"rb");
    if(fp == NULL){
        return 1;
    }
    
    fseek(fp,-size,SEEK_END);
    if(fread(&t,size,1,fp)>0)
        max = t.id;
    fclose(fp);
    return max+1;

}

void item_accept(item_t *e){
    e->id = get_next_id();
    printf("\nName :");
    scanf("%s",e->name);
    printf("Price :");
    scanf("%d",&e->price);
    printf("Quantity :");
    scanf("%lf",&e->quantity);
}

void item_display(item_t *e){
     printf("\n\nID : %d",e->id);
     printf("\nName : %s",e->name);
     printf("\nPrice : %d",e->price);
      printf("\nQuantity : %.2lf",e->quantity);
}
void item_add(item_t *e){
    FILE *fp;
    fp = fopen(ITEM_DB,"ab");
    if(fp == NULL){
        perror("\nCANNOT OPEN FILE");
        return;
    }
    fwrite(e,sizeof(item_t),1,fp);
    printf("\nUser Added");
    fclose(fp);

}
void add_item(){
    item_t e;
    item_accept(&e);
    item_add(&e);
}


void find_item(char name[]){
    FILE *fp;
    item_t t;
    int found = 0;
    fp = fopen(ITEM_DB,"rb");
    if(fp == NULL){
        perror("CanNOT OPEN FILE.");
        return;
    }
    while(fread(&t,sizeof(item_t),1,fp)>0){
        if(strcmp(t.name,name) == 0){
            found = 1;
            item_display(&t);
            break;
        }
    }
    if(!found)
        printf("\nNo such item.");
    fclose(fp);
}

void display_all(){
     FILE *fp;
    item_t t;
    fp = fopen(ITEM_DB,"rb");
    if(fp == NULL){
        perror("CanNOT OPEN FILE.");
        return;
    }
    while(fread(&t,sizeof(item_t),1,fp)>0){
            item_display(&t);
    }

    fclose(fp);
}
void edit_item(){
    int id;
    printf("Enter id of item to be edited :");
    scanf("%d",&id);
    FILE *fp;
    item_t t;
    int found = 0;
    fp = fopen(ITEM_DB,"rb+");
    if(fp == NULL){
        perror("CanNOT OPEN FILE.");
        return;
    }
    while(fread(&t,sizeof(item_t),1,fp)>0){
        if(t.id == id){
            found = 1;
            break;
        }
    }
    if(found){

        item_t i;
        item_accept(&i);
        i.id = t.id;
        fseek(fp,-sizeof(item_t),SEEK_CUR);
        fwrite(&i,sizeof(item_t),1,fp);
        printf("\nITEm UpdatEd");
    }
    else
        printf("\nNo such item.");
    fclose(fp);

}

void delete_item(){
    
    

}
int main(){
    int ch;
    char name[60];
    do{
        
         printf("\n0.EXIT \n1.Add Item\n2.Find Item\n3.Display all \n4.edit item\n5.delete item");
         printf("\n Enter your choice :");
         scanf("%d",&ch);
        switch(ch){
            case 1:
                add_item();
                break;
            case 2:
                printf("Enter name :");
                scanf("%s",name);
                find_item(name);
                break;
            case 3:
                display_all();
                break;
            case 4:
                edit_item();
                break;
            case 5:
                delete_item();
                break;
            
        }
    }while(ch != 0);
    return 0;
}
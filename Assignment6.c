#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE	5

typedef struct student {
	int roll;
	char name[30];
	int std;
	int marks;
}student_t;

typedef struct cirque {
	student_t arr[SIZE];
	int rear;
	int front;
	int count;
}cirque_t;


void accept_student(student_t *s) {
	printf("Roll no : ");
	scanf("%d", &s->roll);
	printf("Name : ");
	scanf("%s", s->name);
	printf("Standard : ");
	scanf("%d", &s->std);
	printf("Marks : ");
	scanf("%d", &s->marks);
}
void display_student(student_t *s) {
	printf("Roll no : %d, Name : %s, standard : %d, Marks :%d\n", s->roll, s->name, s->std, s->marks);
}




void cq_init(cirque_t *q) {
	memset(q->arr, 0, sizeof(q->arr));
	q->front = -1;
	q->rear = -1;
	q->count = 0;
}

void cq_push(cirque_t *q, student_t s) {
	q->rear = (q->rear+1) % SIZE;
	q->arr[q->rear] = s;
	q->count++;
}

void cq_pop(cirque_t *q) {
	q->front = (q->front+1) % SIZE;	
	q->count--;
}

student_t cq_peek(cirque_t *q) {
	int index = (q->front+1) % SIZE;
	return q->arr[index];
}

int cq_empty(cirque_t *q) {
	return q->count == 0;
}

int cq_full(cirque_t *q) {
	return q->count == SIZE;
}

int main() {
	cirque_t q;
	student_t temp;
	int choice;
	cq_init(&q);
	do {
		printf("\n0. EXIT\n1. Push\n2. Pop\n3. Peek\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: 
			if(cq_full(&q))
				printf("Queue is FULL.\n");
			else {
				accept_student(&temp);
				cq_push(&q, temp);
			}
			break;
		case 2: 
			if(cq_empty(&q))
				printf("Queue is EMPTY.\n");
			else {
				temp = cq_peek(&q);
				cq_pop(&q);
				printf("Popped: ");
				display_student(&temp);
			}
			break;
		case 3:
			if(cq_empty(&q))
				printf("Queue is empty.\n");
			else {
				temp = cq_peek(&q);
				printf("Topmost: ");
				display_student(&temp);
			}
			break;
		}
	} while(choice != 0);
	return 0;
}

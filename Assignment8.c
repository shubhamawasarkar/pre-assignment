#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MOVIE_DB "movies.csv"

typedef struct movie{
    int id;
    char name[100];
    char genres[100];
}movie_t;

typedef struct node{
    movie_t data;
    struct node *next;
}node_t;

node_t *head = NULL;

node_t* create_node(movie_t val){
     node_t *newnode = (node_t*)malloc(sizeof(node_t));
    newnode->data = val;
    newnode->next = NULL;
    return newnode;
}

void add_last(movie_t val){
   node_t *newnode = create_node(val);
    if(head == NULL)
        head = newnode;
    else{
        node_t *trav = head;
        while(trav->next !=NULL)
            trav = trav->next;
        trav->next = newnode;
    }
}


void movie_display(movie_t *m){
    printf("id = %d, Name = %s, Genres = %s \n",m->id,m->name,m->genres);
}

void display_list(){
    node_t *trav = head;
    while(trav != NULL){
        movie_display(&trav->data);
        trav = trav->next;
    }
}
int parse_movie(char line[], movie_t *m){
    int success = 0;
    char *id , *name , *genre ;
    id = strtok(line,",\n");
    name = strtok(NULL,",\n");
    genre = strtok(NULL, ",\n");
    if(id == NULL || name == NULL || genre == NULL){
        success = 0;
    }
    else{
        success = 1;
        m->id = atoi(id);
        strcpy(m->name,name);
        strcpy(m->genres,genre);
    }
    return success;
}

void load_movies(){
    char line[1024];
    FILE *fp;
    movie_t m;
    fp = fopen(MOVIE_DB,"r");
    if(fp==NULL){
        perror("CANNOT OPEN FILE.");
        return;
    }
    while(fgets(line,sizeof(line),fp) != NULL){
       //printf("%s",line);
        parse_movie(line,&m);
        //movie_display(&m);
        add_last(m);
    }
    fclose(fp);
}

void find_movie_by_name(){
    char name[100];
    int found = 0;
    node_t *trav = head;
    printf("\nEnter Name to be searched :");
    gets(name);

    while(trav != NULL){
        if(strcmp(name,trav->data.name) == 0){
            movie_display(&trav->data);
            found = 1;
            break;
        }
        trav = trav->next;
    }
    if(!found)
        printf("Movie not found");
}

void find_movie_by_genre(){
    char genre[100];
    int found = 0;
    node_t *trav = head;
    printf("\nEnter Genre to be searched :");
    gets(genre);

    while(trav != NULL){
        if(strstr(trav->data.genres,genre) != NULL){
            movie_display(&trav->data);
            found = 1;
        }
        trav = trav->next;
    }
    if(!found)
        printf("\nMovie not found");
}


int main(){
    load_movies();
    display_list();
    find_movie_by_name();
    find_movie_by_genre();
    return 0;
}